package com.ey.wamacademy.capstoneapi.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ey.wamacademy.capstoneapi.dao.LandingDao;
import com.ey.wamacademy.capstoneapi.model.Landing;

@Service
public class LandingService {
	@Autowired
	private LandingDao landingDao;

	/**
	 * Function to call viewAll from DAO
	 *
	 * @return list to be shown in landing page
	 */
	public List<Landing> viewData() {
		List<Landing> list = landingDao.viewAll();
		return list;
	}

	/**
	 * String formatting to remove double quotes from request the request received
	 * from UI
	 * 
	 * @param instrument
	 * @param country
	 * @param exchange
	 * @return list to be shown in landing page after search filter
	 */
	public List<Landing> viewBySearch(String instrument, String country, String exchange) {
		instrument = instrument.replaceAll("\"", "");
		country = country.replaceAll("\"", "");
		exchange = exchange.replaceAll("\"", "");
		List<Landing> list = landingDao.searchByFilter(instrument, country, exchange);
		return list;
	}

	/**
	 * This function fetches the Instrument name
	 * 
	 * @return list containing Instrument name after invoking the function in
	 *         LandingDao
	 */
	public List<String> getInstrumentName() {
		return landingDao.getInstrumentName();
	}

	/**
	 * This function fetches the Country name
	 * 
	 * @return list containing Country name after invoking the function in
	 *         LandingDao
	 */
	public List<String> getCountryName() {
		return landingDao.getCountryName();
	}

	/**
	 * This function fetches the Exchange name
	 * 
	 * @return list containing Exchange name after invoking the function in
	 *         LandingDao
	 */
	public List<String> getExchangeName() {
		return landingDao.getExchangeName();
	}

}
