package com.ey.wamacademy.capstoneapi.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ey.wamacademy.capstoneapi.dao.HistoricalDao;
import com.ey.wamacademy.capstoneapi.model.Historical;

@Service
public class HistoricalService {

	@Autowired
	private HistoricalDao historicalDao;

	/**
	 * 
	 * This function interacts with the dao class and calls historical data function
	 * 
	 * @param stockId
	 * @return list of historical data
	 */
	public List<Historical> getHistoricalData(int stockId) {
		return historicalDao.getHistoricalData(stockId);
	}

}
