package com.ey.wamacademy.capstoneapi.model;

/**
 * Model class for Historical Data
 *
 * @author Sawan and Anindita
 *
 */
public class Historical {

	private String date;
	private double openPrice;
	private double closePrice;
	private double highPrice;
	private double lowPrice;
	private int dailyTradedVolume;
	private double weekReturn;
	private double monthReturn;
	private double yearReturn;
	private double dailyReturns;

	public Historical() {

	}

	public Historical(String date, double openPrice, double closePrice, double highPrice, double lowPrice,
			int dailyTradedVolume, double weekReturn, double monthReturn, double yearReturn, double dailyReturns) {
		super();
		this.date = date;
		this.openPrice = openPrice;
		this.closePrice = closePrice;
		this.highPrice = highPrice;
		this.lowPrice = lowPrice;
		this.dailyTradedVolume = dailyTradedVolume;
		this.weekReturn = weekReturn;
		this.monthReturn = monthReturn;
		this.yearReturn = yearReturn;
		this.dailyReturns = dailyReturns;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public double getOpenPrice() {
		return openPrice;
	}

	public void setOpenPrice(double openPrice) {
		this.openPrice = openPrice;
	}

	public double getClosePrice() {
		return closePrice;
	}

	public void setClosePrice(double closePrice) {
		this.closePrice = closePrice;
	}

	public double getHighPrice() {
		return highPrice;
	}

	public void setHighPrice(double highPrice) {
		this.highPrice = highPrice;
	}

	public double getLowPrice() {
		return lowPrice;
	}

	public void setLowPrice(double lowPrice) {
		this.lowPrice = lowPrice;
	}

	public int getDailyTradedVolume() {
		return dailyTradedVolume;
	}

	public void setDailyTradedVolume(int dailyTradedVolume) {
		this.dailyTradedVolume = dailyTradedVolume;
	}

	public double getWeekReturn() {
		return weekReturn;
	}

	public void setWeekReturn(double weekReturn) {
		this.weekReturn = weekReturn;
	}

	public double getMonthReturn() {
		return monthReturn;
	}

	public void setMonthReturn(double monthReturn) {
		this.monthReturn = monthReturn;
	}

	public double getYearReturn() {
		return yearReturn;
	}

	public void setYearReturn(double yearReturn) {
		this.yearReturn = yearReturn;
	}

	public double getDailyReturns() {
		return dailyReturns;
	}

	public void setDailyReturns(double dailyReturns) {
		this.dailyReturns = dailyReturns;
	}
}
