package com.ey.wamacademy.capstoneapi.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DbConnections {
	private Logger logger = LoggerFactory.getLogger(DbConnections.class);
	private static DbConnections db = new DbConnections();

	private DbConnections() {
	}

	public static DbConnections getObject() {
		return db;
	}

	/**
	 * Connecting with database
	 * 
	 * @return connection
	 */
	public Connection getConnection() {
		Connection conn = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/StockScreener", "root", "123456");
		} catch (ClassNotFoundException | SQLException ex) {
			logger.debug("Exception Occured" + ex);
		}
		return conn;
	}
}
