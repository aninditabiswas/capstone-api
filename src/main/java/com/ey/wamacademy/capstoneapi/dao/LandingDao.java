package com.ey.wamacademy.capstoneapi.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.ey.wamacademy.capstoneapi.model.Landing;

@Repository
public class LandingDao {
	private Logger logger = LoggerFactory.getLogger(LandingDao.class);
	private PreparedStatement preparedStatement;
	private ResultSet resultSet;

	private static final String VIEW_INSTRUMENT = "select distinct instrument_name from stock_details";
	private static final String VIEW_EXCHANGE = "select distinct e.exchange_code from exchange_details e join stock_details s on s.exchange_id=e.exchange_id";
	private static final String VIEW_COUNTRY = "select distinct c.country_name from country_details c join stock_details s on s.country_id=c.country_id";
	private static final String SELECT_ALL = "select *,(SELECT r.stocks_Return FROM return_details r WHERE r.stock_id = s.stock_id AND r.return_type = 'D' AND r.Price_Effective_Date = p.Price_Effective_Date) AS 'Daily_Returns',(select r.stocks_Return from return_details r where r.stock_id=s.stock_id and r.return_type=\"W\" and r.Price_Effective_Date=p.Price_Effective_Date) as \"1_week_return\",(select r.stocks_Return from return_details r where r.stock_id=s.stock_id and r.return_type=\"M\" and r.Price_Effective_Date=p.Price_Effective_Date) as \"1_month_return\", (select r.stocks_Return from return_details r where r.stock_id=s.stock_id and r.return_type=\"Y\" and r.Price_Effective_Date=p.Price_Effective_Date) as \"1_year_return\" from stock_details s join price_details p on s.stock_id=p.stock_id join country_details co on s.country_id=co.country_id join currency_details c on s.currency_id=c.currency_id join industry_details i on s.industry_id=i.industry_id join instrument_details ins on s.instrument_id=ins.instrument_id join exchange_details e on s.exchange_id=e.exchange_id join sector_details sec on s.sector_id=sec.sector_id where price_effective_date=\"2022-03-30\" order by s.stock_id";

	/**
	 * Landing page data
	 *
	 * @return list of all values to be shown in landing page
	 */
	public List<Landing> viewAll() {
		List<Landing> list = new ArrayList<Landing>();
		try {
			preparedStatement = DbConnections.getObject().getConnection().prepareStatement(SELECT_ALL);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				list = showData(list, resultSet);
			}
		} catch (SQLException e) {
			logger.debug("Exception occurred in viewAll " + e);
		}
		return list;
	}

	/**
	 * Set data in model class
	 *
	 * @param list
	 * @param resultSet
	 * @return list to be show in landing page
	 * @throws SQLException
	 */
	public List<Landing> showData(List<Landing> list, ResultSet resultSet) throws SQLException {
		try {
			Landing landing = new Landing();
			landing.setStockId(resultSet.getInt("stock_id"));
			landing.setDate(resultSet.getString("price_effective_date"));
			landing.setRic(resultSet.getString("ric"));
			landing.setInstrumentName(resultSet.getString("instrument_name"));
			landing.setCurrency(resultSet.getString("currency_code"));
			landing.setIsin(resultSet.getString("isin"));
			landing.setSedol(resultSet.getString("sedol"));
			landing.setTickerSymbol(resultSet.getString("ticker_symbol"));
			landing.setOpenPrice(resultSet.getDouble("open_price"));
			landing.setClosePrice(resultSet.getDouble("close_price"));
			landing.setHighPrice(resultSet.getDouble("high_price"));
			landing.setLowPrice(resultSet.getDouble("low_price"));
			landing.setDailyTradedVolumes(resultSet.getLong("daily_traded_volumes"));
			landing.setWeekReturn(resultSet.getDouble("1_week_return"));
			landing.setMonthReturn(resultSet.getDouble("1_month_return"));
			landing.setYearReturn(resultSet.getDouble("1_year_return"));
			landing.setCompanyMarketCapitalization(resultSet.getLong("company_market_capitalization"));
			landing.setBeta(resultSet.getFloat("beta"));
			landing.setRevenue(resultSet.getFloat("revenue"));
			landing.setEarningsPerShare(resultSet.getDouble("earnings_per_share"));
			landing.setPe(resultSet.getDouble("p_e"));
			landing.setCountryOfExchange(resultSet.getString("country_name"));
			landing.setExchangeName(resultSet.getString("Exchange_code"));
			landing.setSectorName(resultSet.getString("sector_name"));
			landing.setIndustryName(resultSet.getString("industry_name"));
			landing.setInstrumentType(resultSet.getString("instrument_types"));
			landing.setDailyReturns(resultSet.getFloat("daily_returns"));
			list.add(landing);
		} catch (NullPointerException e) {
			logger.debug("Exception occurred in setting data in modelclass " + e);
		}
		return list;
	}

	/**
	 * Filter the data based on the drop down from the UI and returns the result
	 * list.
	 *
	 * @param instrument
	 * @param country
	 * @param exchange
	 * @return list based on search filter
	 */
	public List<Landing> searchByFilter(String instrument, String country, String exchange) {
		List<Landing> list = new ArrayList<Landing>();
		boolean status = false;
		String[] inTemp = instrument.split(",");
		String[] exTemp = exchange.split(",");
		String[] coTemp = country.split(",");
		try {
			if (!exchange.equalsIgnoreCase("null") && !country.equalsIgnoreCase("null")
					&& !instrument.equalsIgnoreCase("null")) {
				int i = 0;
				StringBuilder queryBuilder = searchByThreeField(exTemp, inTemp, coTemp);
				preparedStatement = DbConnections.getObject().getConnection().prepareStatement(queryBuilder.toString());
				logger.debug(queryBuilder.toString());
				for (i = 0; i < exTemp.length; i++) {
					preparedStatement.setString(i + 1, exTemp[i]);
				}
				for (int j = 0; j < inTemp.length; j++) {
					preparedStatement.setString(++i, inTemp[j]);
				}
				for (int j = 0; j < coTemp.length; j++) {
					preparedStatement.setString(++i, coTemp[j]);
				}
				status = true;
			}
			if (status == false && !instrument.equalsIgnoreCase("null") && !exchange.equalsIgnoreCase("null")) {
				int i = 0;
				StringBuilder queryBuilder = searchByTwoField(exTemp, inTemp, "e.Exchange_Code", "s.Instrument_name");
				preparedStatement = DbConnections.getObject().getConnection().prepareStatement(queryBuilder.toString());
				logger.debug(queryBuilder.toString());
				for (i = 0; i < exTemp.length; i++) {
					preparedStatement.setString(i + 1, exTemp[i]);
				}
				for (int j = 0; j < inTemp.length; j++) {
					preparedStatement.setString(++i, inTemp[j]);
				}
				status = true;
			}
			if (status == false && !instrument.equalsIgnoreCase("null") && !country.equalsIgnoreCase("null")) {
				int i = 0;
				StringBuilder queryBuilder = searchByTwoField(coTemp, inTemp, "co.Country_Name", "s.Instrument_name");
				preparedStatement = DbConnections.getObject().getConnection().prepareStatement(queryBuilder.toString());
				logger.debug(queryBuilder.toString());
				for (i = 0; i < coTemp.length; i++) {
					preparedStatement.setString(i + 1, coTemp[i]);
				}
				for (int j = 0; j < inTemp.length; j++) {
					preparedStatement.setString(++i, inTemp[j]);
				}
				status = true;
			}
			if (status == false && !exchange.equalsIgnoreCase("null") && !country.equalsIgnoreCase("null")) {

				int i = 0;
				StringBuilder queryBuilder = searchByTwoField(coTemp, exTemp, "co.Country_Name", "e.Exchange_Code");
				preparedStatement = DbConnections.getObject().getConnection().prepareStatement(queryBuilder.toString());
				logger.debug(queryBuilder.toString());
				for (i = 0; i < coTemp.length; i++) {
					preparedStatement.setString(i + 1, coTemp[i]);
				}
				for (int j = 0; j < exTemp.length; j++) {
					preparedStatement.setString(++i, exTemp[j]);
				}
				status = true;
			}
			if (status == false && !country.equalsIgnoreCase("null")) {
				StringBuilder queryBuilder = searchByOneField(coTemp, "co.Country_Name");
				preparedStatement = DbConnections.getObject().getConnection().prepareStatement(queryBuilder.toString());
				logger.debug(queryBuilder.toString());
				for (int i = 0; i < coTemp.length; i++) {
					preparedStatement.setString(i + 1, coTemp[i]);
				}
				status = true;
			}
			if (status == false && !exchange.equalsIgnoreCase("null")) {

				StringBuilder queryBuilder = searchByOneField(exTemp, "e.Exchange_Code");
				preparedStatement = DbConnections.getObject().getConnection().prepareStatement(queryBuilder.toString());
				logger.debug(queryBuilder.toString());
				for (int i = 0; i < exTemp.length; i++) {
					preparedStatement.setString(i + 1, exTemp[i]);
				}
				status = true;
			}
			if (status == false && !instrument.equalsIgnoreCase("null")) {
				StringBuilder queryBuilder = searchByOneField(inTemp, "s.Instrument_name");
				preparedStatement = DbConnections.getObject().getConnection().prepareStatement(queryBuilder.toString());
				logger.debug(queryBuilder.toString());
				for (int i = 0; i < inTemp.length; i++) {
					preparedStatement.setString(i + 1, inTemp[i]);
				}
				status = true;
			}
			if (status == false) {
				preparedStatement = DbConnections.getObject().getConnection().prepareStatement(SELECT_ALL);
			}
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				list = showData(list, resultSet);
			}
		} catch (SQLException | NullPointerException e) {
			logger.debug("Exception occurred in search dropdown " + e);
		}
		return list;
	}

	/**
	 * Creates dynamic SQL query for 1 drop down at a time
	 *
	 * @param firesultSettfield
	 * @param column
	 * @return query in form of query builder
	 */
	public StringBuilder searchByOneField(String[] firesultSettfield, String column) {
		String query = "select *,(SELECT r.stocks_Return FROM return_details r WHERE r.stock_id = s.stock_id AND r.return_type = 'D' AND r.Price_Effective_Date = p.Price_Effective_Date) AS 'Daily_Returns',(select r.stocks_Return from return_details r where r.stock_id=s.stock_id and r.return_type=\"W\" and r.Price_Effective_Date=p.Price_Effective_Date) as \"1_week_return\",(select r.stocks_Return from return_details r where r.stock_id=s.stock_id and r.return_type=\"M\" and r.Price_Effective_Date=p.Price_Effective_Date) as \"1_month_return\", (select r.stocks_Return from return_details r where r.stock_id=s.stock_id and r.return_type=\"Y\" and r.Price_Effective_Date=p.Price_Effective_Date) as \"1_year_return\" from stock_details s join price_details p on s.stock_id=p.stock_id join country_details co on s.country_id=co.country_id join currency_details c on s.currency_id=c.currency_id join industry_details i on s.industry_id=i.industry_id join instrument_details ins on s.instrument_id=ins.instrument_id join exchange_details e on s.exchange_id=e.exchange_id join sector_details sec on s.sector_id=sec.sector_id where price_effective_date=\"2022-03-30\" and "
				+ column + " in (";
		StringBuilder queryBuilder = new StringBuilder(query);
		for (int i = 0; i < firesultSettfield.length; i++) {
			queryBuilder.append(" ?");
			if (i != firesultSettfield.length - 1)
				queryBuilder.append(",");
		}
		queryBuilder.append(") order by s.stock_id");
		return queryBuilder;
	}

	/**
	 * Creates dynamic SQL query for 2 drop down at a time
	 *
	 * @param field1
	 * @param field2
	 * @param column1
	 * @param column2
	 * @return query in form of query builder
	 */
	public StringBuilder searchByTwoField(String[] field1, String[] field2, String column1, String column2) {
		String query = "select *,(SELECT r.stocks_Return FROM return_details r WHERE r.stock_id = s.stock_id AND r.return_type = 'D' AND r.Price_Effective_Date = p.Price_Effective_Date) AS 'Daily_Returns',(select r.stocks_Return from return_details r where r.stock_id=s.stock_id and r.return_type=\"W\" and r.Price_Effective_Date=p.Price_Effective_Date) as \"1_week_return\",(select r.stocks_Return from return_details r where r.stock_id=s.stock_id and r.return_type=\"M\" and r.Price_Effective_Date=p.Price_Effective_Date) as \"1_month_return\", (select r.stocks_Return from return_details r where r.stock_id=s.stock_id and r.return_type=\"Y\" and r.Price_Effective_Date=p.Price_Effective_Date) as \"1_year_return\" from stock_details s join price_details p on s.stock_id=p.stock_id join country_details co on s.country_id=co.country_id join currency_details c on s.currency_id=c.currency_id join industry_details i on s.industry_id=i.industry_id join instrument_details ins on s.instrument_id=ins.instrument_id join exchange_details e on s.exchange_id=e.exchange_id join sector_details sec on s.sector_id=sec.sector_id where price_effective_date=\"2022-03-30\" and ("
				+ column1 + " in (";
		StringBuilder queryBuilder = new StringBuilder(query);
		for (int i = 0; i < field1.length; i++) {
			queryBuilder.append(" ?");
			if (i != field1.length - 1)
				queryBuilder.append(",");
		}
		queryBuilder.append(")");
		queryBuilder.append(" and " + column2 + " in (");
		for (int i = 0; i < field2.length; i++) {
			queryBuilder.append(" ?");
			if (i != field2.length - 1)
				queryBuilder.append(",");
		}
		queryBuilder.append(")) order by s.stock_id");
		return queryBuilder;
	}

	/**
	 * Creates SQL query for 3 drop down at a time
	 *
	 * @param field1
	 * @param field2
	 * @param field3
	 * @return query in form of query builder
	 */
	public StringBuilder searchByThreeField(String[] field1, String[] field2, String[] field3) {
		String query = "select *,(SELECT r.stocks_Return FROM return_details r WHERE r.stock_id = s.stock_id AND r.return_type = 'D' AND r.Price_Effective_Date = p.Price_Effective_Date) AS 'Daily_Returns',(select r.stocks_Return from return_details r where r.stock_id=s.stock_id and r.return_type=\"W\" and r.Price_Effective_Date=p.Price_Effective_Date) as \"1_week_return\",(select r.stocks_Return from return_details r where r.stock_id=s.stock_id and r.return_type=\"M\" and r.Price_Effective_Date=p.Price_Effective_Date) as \"1_month_return\", (select r.stocks_Return from return_details r where r.stock_id=s.stock_id and r.return_type=\"Y\" and r.Price_Effective_Date=p.Price_Effective_Date) as \"1_year_return\" from stock_details s join price_details p on s.stock_id=p.stock_id join country_details co on s.country_id=co.country_id join currency_details c on s.currency_id=c.currency_id join industry_details i on s.industry_id=i.industry_id join instrument_details ins on s.instrument_id=ins.instrument_id join exchange_details e on s.exchange_id=e.exchange_id join sector_details sec on s.sector_id=sec.sector_id where price_effective_date=\"2022-03-30\" and e.Exchange_Code in (";
		StringBuilder queryBuilder = new StringBuilder(query);
		for (int i = 0; i < field1.length; i++) {
			queryBuilder.append(" ?");
			if (i != field1.length - 1)
				queryBuilder.append(",");
		}
		queryBuilder.append(")");
		queryBuilder.append(" and (s.Instrument_name in (");
		for (int i = 0; i < field2.length; i++) {
			queryBuilder.append(" ?");
			if (i != field2.length - 1)
				queryBuilder.append(",");
		}
		queryBuilder.append(")");
		queryBuilder.append(" and co.Country_Name in (");
		for (int i = 0; i < field3.length; i++) {
			queryBuilder.append(" ?");
			if (i != field3.length - 1)
				queryBuilder.append(",");
		}
		queryBuilder.append(")) order by s.stock_id");
		return queryBuilder;
	}

	/**
	 * This function fetches the Instrument name
	 * 
	 * @return list containing Instrument name
	 */
	public List<String> getInstrumentName() {
		List<String> list = new ArrayList<String>();
		try {
			preparedStatement = DbConnections.getObject().getConnection().prepareStatement(VIEW_INSTRUMENT);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				list.add(resultSet.getString(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
		}
		return list;
	}

	/**
	 * This function fetches the Exchange name
	 * 
	 * @return list containing Exchange name
	 */
	public List<String> getExchangeName() {
		List<String> list = new ArrayList<String>();
		try {
			preparedStatement = DbConnections.getObject().getConnection().prepareStatement(VIEW_EXCHANGE);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				list.add(resultSet.getString(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	/**
	 * This function fetches the Country name
	 * 
	 * @return list containing Country name
	 */
	public List<String> getCountryName() {
		List<String> list = new ArrayList<String>();
		try {
			preparedStatement = DbConnections.getObject().getConnection().prepareStatement(VIEW_COUNTRY);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				list.add(resultSet.getString(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

}
