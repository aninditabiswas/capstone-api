package com.ey.wamacademy.capstoneapi.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.ey.wamacademy.capstoneapi.model.Historical;

@Repository
public class HistoricalDao {

	private Logger logger = LoggerFactory.getLogger(HistoricalDao.class);
	private PreparedStatement preparedStatement;
	private ResultSet resultSet;

	/**
	 * 
	 * This function fetches the historical data
	 * 
	 * @param stockId
	 * @return list of historical data
	 */

	public List<Historical> getHistoricalData(int stockId) {
		List<Historical> list = new ArrayList<Historical>();
		try {
			preparedStatement = DbConnections.getObject().getConnection().prepareStatement(
					"select *,(SELECT r.stocks_return FROM return_details r WHERE r.stock_id = s.stock_id AND r.return_type = 'd' AND r.Price_Effective_Date = p.Price_Effective_Date) AS 'Daily_Returns',(select r.stocks_Return from return_details r where r.stock_id=s.stock_id and r.return_type=\"W\" and r.Price_Effective_Date=p.Price_Effective_Date) as \"1_week_return\",(select r.stocks_Return from return_details r where r.stock_id=s.stock_id and r.return_type=\"M\" and r.Price_Effective_Date=p.Price_Effective_Date) as \"1_month_return\", (select r.stocks_Return from return_details r where r.stock_id=s.stock_id and r.return_type=\"Y\" and r.Price_Effective_Date=p.Price_Effective_Date) as \"1_year_return\" from stock_details s join price_details p on s.stock_id=p.stock_id where s.stock_id=? and p.price_effective_date!=\"2022-03-30\"");
			preparedStatement.setInt(1, stockId);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				Historical historical = new Historical();
				historical.setDate(resultSet.getString("Price_Effective_Date"));
				historical.setClosePrice(resultSet.getDouble("close_price"));
				historical.setDailyTradedVolume(resultSet.getInt("daily_traded_volumes"));
				historical.setHighPrice(resultSet.getDouble("high_price"));
				historical.setLowPrice(resultSet.getDouble("low_price"));
				historical.setOpenPrice(resultSet.getDouble("open_price"));
				historical.setMonthReturn(resultSet.getDouble("1_month_return"));
				historical.setWeekReturn(resultSet.getDouble("1_week_return"));
				historical.setYearReturn(resultSet.getDouble("1_year_return"));
				historical.setDailyReturns(resultSet.getDouble("Daily_Returns"));
				list.add(historical);
			}
		} catch (Exception e) {
			logger.debug("Error occured in viewAllHistory Function" + e);
		}
		return list;
	}

}
