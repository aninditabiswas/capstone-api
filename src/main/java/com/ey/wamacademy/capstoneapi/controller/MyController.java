package com.ey.wamacademy.capstoneapi.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.ey.wamacademy.capstoneapi.model.Historical;
import com.ey.wamacademy.capstoneapi.model.Landing;
import com.ey.wamacademy.capstoneapi.services.HistoricalService;
import com.ey.wamacademy.capstoneapi.services.LandingService;

@RestController
@CrossOrigin
public class MyController {
	Logger logger = LoggerFactory.getLogger(MyController.class);
	@Autowired
	private LandingService landingService;
	@Autowired
	private HistoricalService historicalService;

	/**
	 * Controller for viewAll
	 * 
	 * @return list to be shown in landing page
	 * 
	 */
	@GetMapping("viewall")
	public List<Landing> viewAll() {
		logger.debug("ViewAll initiated from controller");
		return landingService.viewData();
	}

	/**
	 * Controller for drop down search filter
	 * 
	 * @param instrument
	 * @param country
	 * @param exchange
	 * @return list for the landing page after search filter
	 */
	@GetMapping("/view/{instrument}/{country}/{exchange}")
	public List<Landing> findBySearch(@PathVariable String instrument, @PathVariable String country,
			@PathVariable String exchange) {
		logger.debug("Search initiated from controller");
		return landingService.viewBySearch(instrument, country, exchange);
	}

	/**
	 * Controller for getInstrumentNname
	 * 
	 * @return list that contains Instrument Name
	 */
	@GetMapping("getInstrumentName")
	public List<String> getInstrumentName() {
		return landingService.getInstrumentName();
	}

	/**
	 * Controller for getCountryName
	 * 
	 * @return list that contains Country Name
	 */
	@GetMapping("getCountryName")
	public List<String> getCountryName() {
		return landingService.getCountryName();
	}

	/**
	 * Controller for getExchangeName
	 * 
	 * @return list that contains Exchange Name
	 */
	@GetMapping("getExchangeName")
	public List<String> getExchangeName() {
		return landingService.getExchangeName();
	}

	/**
	 * 
	 * Controller for fetching historical data with a stock_id
	 * 
	 * @param stockId
	 * @return list of historical data
	 */
	@GetMapping("getHistorical/{stockId}")
	public List<Historical> getHistoricalData(@PathVariable int stockId) {
		return historicalService.getHistoricalData(stockId);
	}
}