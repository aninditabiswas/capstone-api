package com.ey.wamacademy.capstoneapi.controller;

import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.ey.wamacademy.capstoneapi.model.Historical;
import com.ey.wamacademy.capstoneapi.model.Landing;
import com.ey.wamacademy.capstoneapi.services.HistoricalService;
import com.ey.wamacademy.capstoneapi.services.LandingService;

@WebMvcTest(value = MyController.class)
class MyControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private LandingService mockLandingservice;

	@MockBean
	private HistoricalService mockHistoricalService;

	/**
	 * Test for viewall function in MyController
	 * 
	 * 
	 * @throws Exception
	 */
	@Test
	void testViewAll() throws Exception {

		List<Landing> mockLandingList = new ArrayList<Landing>();
		Landing land = new Landing();
		land.setClosePrice(4d);
		mockLandingList.add(land);

		Mockito.when(mockLandingservice.viewData()).thenReturn(mockLandingList);

		MvcResult mvcResult = this.mockMvc.perform(get("/viewall").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();

		assertTrue(mvcResult.getResponse().getContentAsString().contains("4.0"));
	}

	/**
	 * test for exchangeName
	 * 
	 * @throws Exception
	 */
	@Test
	void testExchangeName() throws Exception {

		List<String> mockLandingList = new ArrayList<String>();
		mockLandingList.add("Dummy");
		Mockito.when(mockLandingservice.getExchangeName()).thenReturn(mockLandingList);

		MvcResult mvcResult = this.mockMvc.perform(get("/getExchangeName").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();

		assertTrue(mvcResult.getResponse().getContentAsString().contains("Dummy"));
	}

	/**
	 * test for intrumentName
	 * 
	 * @throws Exception
	 */
	@Test
	void testInstrumentName() throws Exception {

		List<String> mockLandingList = new ArrayList<String>();
		mockLandingList.add("Dummy");
		Mockito.when(mockLandingservice.getInstrumentName()).thenReturn(mockLandingList);

		MvcResult mvcResult = this.mockMvc.perform(get("/getInstrumentName").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();

		assertTrue(mvcResult.getResponse().getContentAsString().contains("Dummy"));
	}

	/**
	 * test for countryName
	 * 
	 * @throws Exception
	 */
	@Test
	void testCountryName() throws Exception {

		List<String> mockLandingList = new ArrayList<String>();
		mockLandingList.add("Dummy");
		Mockito.when(mockLandingservice.getCountryName()).thenReturn(mockLandingList);

		MvcResult mvcResult = this.mockMvc.perform(get("/getCountryName").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();

		assertTrue(mvcResult.getResponse().getContentAsString().contains("Dummy"));
	}

	/**
	 * test for searchFilter
	 * 
	 * @throws Exception
	 */
	@Test
	void testSearchFilter() throws Exception {

		List<Landing> mockLandingList = new ArrayList<Landing>();
		Landing land = new Landing();
		land.setInstrumentName("Dummy");
		mockLandingList.add(land);
		Mockito.when(mockLandingservice.viewBySearch("dummy", "dummy", "dummy")).thenReturn(mockLandingList);

		MvcResult mvcResult = this.mockMvc
				.perform(get("/view/dummy/dummy/dummy").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();

		assertTrue(mvcResult.getResponse().getContentAsString().contains("Dummy"));
	}

	/**
	 * test for historicalData
	 * 
	 * @throws Exception
	 */
	@Test
	void testHistoricalData() throws Exception {

		List<Historical> mockHistoricalList = new ArrayList<Historical>();
		Historical hist = new Historical();
		hist.setClosePrice(4d);
		mockHistoricalList.add(hist);
		Mockito.when(mockHistoricalService.getHistoricalData(1)).thenReturn(mockHistoricalList);

		MvcResult mvcResult = this.mockMvc.perform(get("/getHistorical/1").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();

		assertTrue(mvcResult.getResponse().getContentAsString().contains("4.0"));
	}
}
