package com.ey.wamacademy.capstoneapi.dao;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.ey.wamacademy.capstoneapi.model.Landing;
import com.ey.wamacademy.capstoneapi.services.LandingService;

@SpringBootTest
class LandingDaoTest {

	@SuppressWarnings("serial")
	List<Landing> records = new ArrayList<Landing>() {
		{
			add(new Landing(1, "30 March 2022", "/BAM", "Brookfield Asset Management Ord Shs Class A", "USD",
					"CA1125851040", "2092555", "BAM", 57.86, 57.61, 58.35, 57.38, 859012L, 3.8, 5.4, 31.6, 9.2, 1.2f,
					16.8f, 2.39, 23.8, "United States of America", "NYSE", "Financials", "Capital Markets",
					"Ordinary Shares", 1.2f));
			add(new Landing(2, "30 March 2022", "/BAM", "Brookfield Asset Management Ord Shs Class A", "USD",
					"CA1125851040", "2092555", "BAM", 57.86, 57.61, 58.35, 57.38, 859012L, 3.8, 5.4, 31.6, 9.2, 1.2f,
					16.8f, 2.39, 23.8, "United States of America", "NYSE", "Financials", "Capital Markets",
					"Ordinary Shares", 1.2f));
		}
	};

	@SuppressWarnings("serial")
	List<String> uniqueExchanges = new ArrayList<String>() {
		{
			add("NYSE");
			add("TSX");
			add("NASDAQ");
			add("SSE");
		}
	};

	@SuppressWarnings("serial")
	List<String> uniqueCountries = new ArrayList<String>() {
		{
			add("United States of America");
			add("Belgium");
			add("Australia");
			add("Canada");
		}
	};

	@SuppressWarnings("serial")
	List<String> uniqueIdustries = new ArrayList<String>() {
		{
			add("Brookfield Asset Management Ord Shs Class A");
			add("CME Group Ord Shs Class A");
			add("UBS Group Ord Shs");
			add("Moody's Ord Shs");
		}
	};

	@Autowired
	private LandingService landingService;

	@MockBean
	private LandingDao LandingDao;

	/**
	 * Method for testing that empty result is not returned by viewAll method
	 * 
	 */
	@Test
	void viewAllTest() {
		when(LandingDao.viewAll()).thenReturn(records);
		assertThat(landingService.viewData().size()).isGreaterThan(0);
	}

	/**
	 * Method for verifying unique country values returned
	 * 
	 */
	@Test
	void getUniqueCountryNamesTest() {
		when(LandingDao.getCountryName()).thenReturn(uniqueCountries);
		List<String> actual = landingService.getCountryName();
		List<String> expected = Arrays.asList("United States of America", "Belgium", "Australia", "Canada");
		assertTrue(actual.size() == expected.size() && actual.containsAll(expected) && expected.containsAll(actual));
	}

	/**
	 * Method for verifying unique exchange values returned
	 * 
	 */
	@Test
	void getUniqueExchangeNamesTest() {
		when(LandingDao.getExchangeName()).thenReturn(uniqueExchanges);
		List<String> actual = landingService.getExchangeName();
		List<String> expected = Arrays.asList("NYSE", "TSX", "NASDAQ", "SSE");
		assertTrue(actual.size() == expected.size() && actual.containsAll(expected) && expected.containsAll(actual));
	}

	/**
	 * Method for verifying unique industry values returned
	 * 
	 */
	@Test
	void getUniqueIndustryNamesTest() {
		when(LandingDao.getInstrumentName()).thenReturn(uniqueIdustries);
		List<String> actual = landingService.getInstrumentName();
		List<String> expected = Arrays.asList("Brookfield Asset Management Ord Shs Class A",
				"CME Group Ord Shs Class A", "UBS Group Ord Shs", "Moody's Ord Shs");
		assertTrue(actual.size() == expected.size() && actual.containsAll(expected) && expected.containsAll(actual));
	}

}
