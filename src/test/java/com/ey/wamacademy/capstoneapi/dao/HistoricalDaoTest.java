package com.ey.wamacademy.capstoneapi.dao;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.ey.wamacademy.capstoneapi.model.Historical;
import com.ey.wamacademy.capstoneapi.services.HistoricalService;

@SpringBootTest
class HistoricalDaoTest {

	@Autowired
	private HistoricalService mockHistoricalService;

	@MockBean
	private HistoricalDao mockHistoricalDao;

	@SuppressWarnings("serial")
	List<Historical> records = new ArrayList<Historical>() {
		{

			add(new Historical("dummy", 1.0, 2.0, 3.0, 4.0, 5, 6.0, 7.0, 8.0, 9.0));
		}
	};

	/**
	 * Method for testing historical dao function
	 */
	@Test
	void testHistorical() {
		when(mockHistoricalDao.getHistoricalData(1)).thenReturn(records);
		assertThat(mockHistoricalService.getHistoricalData(1).size()).isGreaterThan(0);
	}
}
